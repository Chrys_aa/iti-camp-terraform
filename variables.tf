variable "availability_zone" {
    default = [
        "us-east-1a",
        "us-east-1b",
        "us-east-1c",
    ]
}

variable "cdirs-ssh-access" {
  type = list
  default = ["177.76.98.116/32"]
}

variable "FullAccess" {
  type = list
  default = ["0.0.0.0/0"]
}