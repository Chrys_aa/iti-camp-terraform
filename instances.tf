resource "aws_instance" "instances" {
  count         = 3

  ami           = "ami-09d95fab7fff3776c"
  instance_type = "t2.micro"
  key_name      = "terraform-aws"

  subnet_id = element(aws_subnet.public_subnet.*.id, count.index)
  tags = {
  Name = "iti_instances_${count.index}"
  }

  vpc_security_group_ids = [aws_security_group.acessoSSH.id, aws_security_group.allow_outbound.id, aws_security_group.RDS_SG_5432.id, aws_security_group.load_balancer_tg.id]
  user_data = templatefile("${path.module}/user_data/install_app.tmpl", {database_endpoint = module.rds.this_db_instance_endpoint})
}

output "public_ips" {
    value = join(", ", aws_instance.instances.*.public_ip)
}