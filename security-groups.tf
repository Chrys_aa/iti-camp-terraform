resource "aws_security_group" "acessoSSH" {
  vpc_id = aws_vpc.main.id
  name = "iti_PermitIngressSSH"
  description = "Permits Ingress on SSH from an access list"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    # Lista de IP's no padrão CIDR para liberação
    cidr_blocks = var.cdirs-ssh-access
  }
  egress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    # Lista de IP's no padrão CIDR para liberação
    cidr_blocks = var.FullAccess
  }
  tags = {
    name = "ssh"
  }
}

resource "aws_security_group" "RDS_SG_5432" {
  vpc_id = aws_vpc.main.id
  name = "iti_rds_SG_5432"
  description = "Used for iti RDS database on port 5432"

  ingress {
    from_port = 5432
    to_port = 5432
    protocol = "tcp"
    self = true
  }

  tags = {
    name = "ssh"
  }
}

resource "aws_security_group" "allow_outbound" {
  vpc_id = aws_vpc.main.id
  name = "iti_allow_outbound"
  description = "Permit outbound to all servers"

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    name = "allow_outbound"
  }
}

resource "aws_security_group" "load_balancer_ingress"{
    vpc_id = aws_vpc.main.id
    name = "iti_load_balancer_ingress"

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

    egress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        security_groups = [aws_security_group.load_balancer_tg.id]
    }
}

resource "aws_security_group" "load_balancer_tg" {
    vpc_id = aws_vpc.main.id
    name = "iti_load_balancer_tg"

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        self = true
    }
}